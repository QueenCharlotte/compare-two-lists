var twoCompare = {};
function isUnique(value,index,self) {
    return self.indexOf(value) == index;
}

function getAndSplit(identifier) {
    return document.querySelector("#"+identifier).value.split("\n");
}

function toString (accumulator, value) {
    return accumulator + "\n" + value;
}

function reduceAndPut(identifier, list) {
    document.querySelector("#"+identifier).value = list.reduce(toString);
}

function gatherLists() {
    twoCompare.caseInsensitive = document.querySelector("#caseInsensitive").checked;
    getList = getAndSplit;
    if (twoCompare.caseInsensitive) {
        getList = function(id) {
            return getAndSplit(id).map((value)=>value.toLocaleLowerCase())
        }
    }
    twoCompare.listOne = getList("listOne").filter(isUnique);
    twoCompare.listTwo = getList("listTwo").filter(isUnique);
    twoCompare.listOneOnly = [];
    twoCompare.listTwoOnly = [];
    twoCompare.listOneAndTwo = [];
    twoCompare.listOneOrTwo = [];
}

function doComparison() {
    twoCompare.listOne.sort();
    twoCompare.listTwo.sort();

    listOne = twoCompare.listOne;
    listTwo = twoCompare.listTwo;
    
    i = 0;
    j = 0;
    while ( i < listOne.length && j < listTwo.length ) {
        comparison = listOne[i].localeCompare(listTwo[j]);
        if (comparison < 0) {
            twoCompare.listOneOnly.push(listOne[i]);
            twoCompare.listOneOrTwo.push(listOne[i]);
            i += 1;
        } else if (comparison == 0) {
            twoCompare.listOneAndTwo.push(listOne[i]);
            twoCompare.listOneOrTwo.push(listOne[i])
            i += 1;
            j += 1;
        } else {
            twoCompare.listTwoOnly.push(listTwo[j]);
            twoCompare.listOneOrTwo.push(listTwo[j])
            j += 1;
        }
    }
    if (i < listOne.length) {
        extraElements = listOne.slice(i);
        twoCompare.listOneOnly = twoCompare.listOneOnly.concat(extraElements);
        twoCompare.listOneOrTwo = twoCompare.listOneOrTwo.concat(extraElements);
    }
    if (j < listTwo.length) {
        extraElements = listTwo.slice(j);
        twoCompare.listTwoOnly = twoCompare.listTwoOnly.concat(extraElements);
        twoCompare.listOneOrTwo = twoCompare.listOneOrTwo.concat(extraElements);
    }
}

function outputResults() {
    reduceAndPut("listOne", twoCompare.listOne);
    reduceAndPut("listTwo", twoCompare.listTwo);
    reduceAndPut("listOneOnly", twoCompare.listOneOnly);
    reduceAndPut("listTwoOnly", twoCompare.listTwoOnly);
    reduceAndPut("listOneAndTwo", twoCompare.listOneAndTwo);
    reduceAndPut("listOneOrTwo", twoCompare.listOneOrTwo);    
}

function compareTwoLists() {
    gatherLists();
    doComparison();
    outputResults();
}